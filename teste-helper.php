<?php require('couch/cms.php'); ?>
<cms:embed "html/tag-html.php" />
<cms:template title="Teste Helper" order="25" />
<head>
	<?php
		// <cms:set src_page_title="" />
		// <cms:set src_page_description="" />
		// <cms:set src_page_keywords="" />
		// <cms:set custom_css="home.css" />
		// <cms:set custom_js="home.js" />
	?>
	<cms:embed "html/tag-head.php" />
</head>
<body>
	<cms:embed "structure/header.php" />
	<h1>
		Teste Helper
	</h1>

	<cms:set hlp_base_button_value="valor do botão 1" />
	<cms:embed "helpers/base-button.php" />

	<cms:set hlp_base_button_value="valor do botão 2" />
	<cms:embed "helpers/base-button.php" />

	<cms:set hlp_base_button_value="" />
	<cms:embed "helpers/base-button.php" />

	<cms:embed "structure/footer.php" />
	<cms:embed "html/tag-foot.php" />
</body>
</html>
<?php COUCH::invoke(); ?>
