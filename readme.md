# Hyojun.couch-bootstrap

`versão 0.1.0`

Bootstrap para projetos estáticos com CMS. Utilizando CouchCMS (php + mysql) utilizados pela F.biz.

## Setup

Instale os seguintes programas:

1. [Ruby](http://www.ruby-lang.org/pt/downloads/) — `2.0+` ou `brew install ruby`*;
2. [Vagrant](https://www.vagrantup.com/downloads.html) — `1.7.3+` ou `brew cask install vagrant`*;
3. [Virtual Box 4](https://www.virtualbox.org/wiki/Download_Old_Builds_4_3) — `4.3.30+` ou `brew cask install virtualbox`*;
3. [NodeJS](http://nodejs.org) — `0.12+` ou `brew install nodejs`*;
4. Bundler — `gem install bundler`;
5. Bower — `npm install -g bower`;
6. GruntCLI — `npm install -g grunt-cli`;

_*No caso do Mac com [homebrew](http://brew.sh/) + [cask](http://caskroom.io/) instalado._

Clone o projeto e rode as tarefas:

    $ git clone git@bitbucket.org:fbiz/hyojun.couch-bootstrap.git
    $ cd hyojun.couch-bootstrap/ && npm install && grunt setup

Quando a máquina virtual estiver pronta, o servidor estará disponível em http://localhost:1144/

Para acessar o admin do CouchCMS, entre em http://localhost:1144/couch/adm.php com o login `system` e senha `admin`.

Veja a documentação completa na [wiki](https://bitbucket.org/fbiz/hyojun.couch-bootstrap/wiki/Home).

## Como contribuir

Participe das conversas no slack e na lista de issues: discuta, abra, investigue ou resolva issues. Existem diversas melhorias a serem feitas! :)

Antes de começar a produzir, converse e veja se não há outra pessoa trabalhando na mesma coisa. Fique por dentro dos milestones do projeto.

Faça um fork do projeto e [envie um pull-request](https://bitbucket.org/fbiz/hyojun.couch-bootstrap/pull-request/new) para o branch `working`.

Utilizamos [semver](http://semver.org/) para versionar o projeto e abrimos uma tag no formato "major.minor.fix" para cada release público.

Canal do slack: https://fbiz.slack.com/messages/hyojun/
