module.exports = function(grunt) {

	'use strict';

	require('jit-grunt')(grunt, {
		scsslint: 'grunt-scss-lint'
	});

	if (grunt.option('verbose')) {
		require('time-grunt')(grunt);
	}

	grunt.config.set('paths', {
		project: (function() {
			var run = require('child_process').execSync;
			var tplName = 'hyojun_couch_bootstrap';
			var name = '';
			try {
				name = run('git remote -v 2> /dev/null').toString('utf8');
				name = (name.match(/\/(.[^\/]*)\.git/) || [name]).pop();
				name = name.replace(/[^a-zA-Z0-9]/g, '_');
			} catch (e) {}

			return !!name ? name : tplName;
		}()),
		rjs: {
			bower: '../../bower_components'
		}
	});

	grunt.config.set('watch', {
		build: {
			files: [
				'**/*.php',
				'!couch/**/*.php'
			],
			tasks: ['exec:sync', 'build', 'exec:touch'],
			options: {
				livereload: true,
				spawn: false
			}
		},
		requirejs_partial: {
			files: [
				'assets/**/*.js',
				'!assets/js/dist/**/*.js'
			],
			tasks: ['requirejs_partial', 'exec:sync', 'exec:touch'],
			options: {
				livereload: true,
				spawn: false
			}
		},
		sass: {
			files: ['assets/sass/**/*.scss'],
			tasks: ['sass:dev', 'exec:sync', 'exec:touch']
		},
		noop: {
			options: {
				livereload: true
			},
			files: 'noop.config'
		}
	});

	// tasks estão separadas no diretório grunt
	grunt.loadTasks('grunt');

	grunt.registerTask('setup',
		'Faz o startup da máquina, deixando-a disponível em localhost:1144/;',
		[
			'githooks', 'exec:bower', 'exec:bundle',
			'exec:vm-start', 'exec:vm-mysql',
			'update-config', 'css', 'js',
			'sync', 'couch-install', 'build'
		]);

	grunt.registerTask('restore',
		[
			'Faz a restauração baseado no arquivo ZIP gerado pela task "backup":',
			'* --file - caminho do arquivo relativo à posição do gruntfile;'
		].join('\n'),
		[
			'backup', 'restore-zip', 'exec:vm-mysqldrop',
			'exec:vm-mysql', 'sync', 'couch-install', 'build', 'clean-restore'
		]);

	grunt.registerTask('backup',
		'Gera o ZIP de backup com o dump do banco de dados e imagens usadas no site',
		['fetch-bkp', 'get-dump', 'generate-backup-zip']);

	grunt.registerTask('lint',
		'Valida o projeto com jshint e scsslint;',
		['scsslint', 'jshint']);

	grunt.registerTask('default',
		'Task padrão gera o css e js no modo "produção".',
		['css', 'js']);
};
