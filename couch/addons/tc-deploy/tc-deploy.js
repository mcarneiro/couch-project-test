/* Promise Polyfill */
!window.Promise && !function(a){"use strict";function f(a){setTimeout(a,0)}function g(a,b){return function(){for(var c;c=a.shift();)c(b)}}function h(a,c,d,e){var f;if(c===a)throw new TypeError("A promise's fulfillment handler cannot return the same promise");if(c instanceof b)c.then(d,e);else if(!c||"object"!=typeof c&&"function"!=typeof c)d(c);else{try{f=c.then}catch(g){return void e(g)}if("function"==typeof f){var i,j,k;j=function(b){i||(i=!0,h(a,b,d,e))},k=function(a){i||(i=!0,e(a))};try{f.call(c,j,k)}catch(g){if(!i)return e(g),void(i=!0)}}else d(c)}}var b,c={},d={},e={};"function"==typeof a.Promise?b=a.Promise:(b=function(a){var l,m,n,o,p,q,i=[],j=[],k=c;return n=function(a){return function(b){k===c&&(l=b,k=a,m=g(k===d?i:j,l),f(m))}},o=n(d),p=n(e),a(o,p),q={then:function(a,d){var e=new b(function(b,g){var l=function(a,c,d){"function"==typeof a?c.push(function(c){var d;try{d=a(c),h(e,d,b,g)}catch(f){g(f)}}):c.push(d)};l(a,i,b),l(d,j,g),k!==c&&f(m)});return e}},q["catch"]=function(a){return this.then(null,a)},q},b.all=function(a){return new b(function(b,c){var e,f,g,d=[];if(!a.length)return void b(d);for(g=function(f){a[f].then(function(a){d[f]=a,--e||b(d)},c)},e=f=a.length;f--;)g(f)})},b.race=function(a){return new b(function(b,c){a.forEach(function(a){a.then(b,c)})})},b.resolve=function(a){return new b(function(b){b(a)})},b.reject=function(a){return new b(function(b,c){c(a)})}),"undefined"!=typeof module&&module.exports?module.exports=b:"function"==typeof define&&define.amd&&define(function(){return b}),a.Promise=b}("undefined"!=typeof window?window:this);

(function(scope) {

	function TcDeploy(holder) {
		this.holder = holder;
		this.status = 'iddle';
		this.tcStatus = 'iddle';
		this.confirmMsg = 'Do you really want to publish? It might take some minutes.';

		this.loadConfig()
			.then(function() {
				this.createElements();
				this.addElements();
				this.addVerifier();
				this.addEvents();
			}.bind(this))
			.catch(function(err) {
				console.warn(err);
			});
	}

	TcDeploy.prototype = {
		createElements: function() {
			this.link = new Element('a', {
				href: this.apiRoot + 'action.html?add2Queue=' + this.buildId,
				html: 'Publicar'
			});
			this.feedback = new Element('div', {
				'class': 'tc-feedback'
			});
			this.loading = new Element('object', {
				id: 'loading',
				type: 'image/svg+xml',
				data: 'addons/tc-deploy/tc-loading-icon.svg'
			});
			this.elm = new Element('li', {
				'class': 'tc-deploy'
			});
			this.link.inject(this.elm);
			this.loading.inject(this.elm);
			this.feedback.inject(this.holder[0], 'bottom');
			this.elmSep = new Element('li', {
				html: '|'
			});
		},

		addElements: function() {
			var children = this.holder.getChildren('li')[0];
			var child = children[children.length - 3];

			this.elm.inject(child, 'after');
			this.elmSep.inject(child, 'after');
		},

		addVerifier: function() {
			if (this.status === 'iddle') {
				clearTimeout(this.timeout);
				this.timeout = setTimeout(this.callTc.bind(this), 2000);
			}
		},

		addEvents: function() {
			this.link.addEvent('click', function(e) {
				if (this.tcStatus === 'iddle' && confirm(this.confirmMsg)) {
					this.sendBuildCommand();
				}
				e.preventDefault();
			}.bind(this));
		},

		sendBuildCommand: function() {
			var request = new Request({
				url: this.link.get('href'),
				method: 'get',
				headers: {
					'authorization': 'Basic ' + this.auth
				},

				onSuccess: function() {
					this.tcStatus = 'queued';
				}.bind(this),
				onFailure: function() {
					this.tcStatus = 'queued';
				}.bind(this)
			});
			request.send();
			this.feedback.set('html', '');
			this.feedback.removeClass('success');
			this.feedback.removeClass('fail');
			this.tcStatus = 'loading';
			this.loading.addClass('show');
		},

		loadFinalStatus: function() {

			var request = new Request.JSON({
				url: this.apiRoot + this.buildHref,
				method: 'get',
				headers: {
					'authorization': 'Basic ' + this.auth
				},
				onSuccess: function(data) {

					if (data.state !== "finished") {
						this.loadFinalStatus();
						return;
					}

					var status = data.status === 'SUCCESS' ? 'Publicado com sucesso' : 'Erro ao publicar';

					this.feedback.set('html', status);
					this.feedback.removeClass('success');
					this.feedback.removeClass('fail');
					this.feedback.addClass(data.status === 'SUCCESS' ? 'success' : 'fail');

				}.bind(this)
			});
			request.send();
		},

		loadConfig: function() {
			return new Promise(function(ok, fail) {
				var request = new Request.JSON({
					url: 'addons/tc-deploy/tc-deploy-config.json',
					onSuccess: function(data) {
						this.apiRoot = data['api-root'];
						this.projectId = data['project-id'];
						this.buildId = data['build-id'];
						this.auth = data.auth;
						this.confirmMsg = data['confirm-message'] || this.confirmMsg;
						this.statucIconUrl = [
							this.apiRoot,
							'app/rest/builds/buildType:(id:',
							this.buildId,
							',count:1)/statusIcon'
						].join('');

						if (this.auth) {
							ok();
						} else {
							fail('Rodando local sem informação do TeamCity. Ignorando botão de publish.');
						}
					}.bind(this),
					onError: fail
				});
				request.get();
			}.bind(this));
		},

		callTc: function() {
			var url = this.apiRoot + '/app/rest/builds/?locator=project:' + this.projectId + ',running:true';
			this.request = new Request.JSON({
				url: url,
				headers: {
					'authorization': 'Basic ' + this.auth
				},
				withCredentials: true,
				onSuccess: function(res) {
					this.status = 'iddle';
					this.onCallTc(res);
					this.addVerifier();
				}.bind(this),

				onError: function() {
					this.status = 'iddle';
					this.addVerifier();
				}.bind(this)
			});
			this.request.get();
			this.status = 'running';
		},

		onCallTc: function(data) {
			var hasRunningBuild = false;
			if (data.count === 0) {
				if (this.wasRunning) {
					this.wasRunning = false;
					this.feedback.addClass('show');
					this.loadFinalStatus();
					this.tcStatus = 'iddle';
				}

				switch (this.tcStatus) {
					case 'iddle':
						this.link.set('html', 'Publicar');
						this.loading.removeClass('show');
						break;
					case 'queued':
						this.link.set('html', 'Publicação na fila');
						this.loading.addClass('show');
						break;
				}
				return;
			}

			data.build.forEach(function(val) {
				if (val.buildTypeId === this.buildId && val.running) {
					hasRunningBuild = true;
					this.buildHref = val.href;
				}
			}.bind(this));

			if (hasRunningBuild) {
				this.wasRunning = true;
				this.tcStatus = 'running';
				this.link.set('html', 'Publicando');
				this.loading.addClass('show');
			}
		}
	}

	scope.addEvent('domready', function() {
		scope.hcTcDeploy = new TcDeploy($$('#admin-subnav'));
	});

}(window));
