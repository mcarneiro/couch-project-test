<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<cms:if (k_page_id != "") && (k_page_title == "")>
	<cms:editable name="page_title"
		label="Title"
		type="text"
		hidden="1"
		order="0"><cms:show src_page_title /></cms:editable>
</cms:if>
<cms:if page_title == "">
	<cms:set page_title="<cms:show k_page_title />" />
</cms:if>

<cms:editable name="page_description"
	label="Descrição da página (metatag)"
	type="text"
	hidden="1"
	order="0"><cms:show src_page_description /></cms:editable>
<cms:if page_description == "">
	<cms:set page_description="<cms:get_custom_field 'site_description' masterpage='global.php' />" />
</cms:if>

<cms:editable name="page_keywords"
	label="Palavras-chave do Site (metatag)"
	type="text"
	hidden="1"
	order="0"><cms:show src_page_keywords /></cms:editable>
<cms:if page_keywords == "">
	<cms:set page_keywords="<cms:get_custom_field 'site_keywords' masterpage='global.php' />" />
</cms:if>

<title><cms:show page_title /> - <cms:get_custom_field "site_title" masterpage="globals.php" /> </title>
<meta charset="utf-8">
<meta name="description" content="<cms:show page_description />">
<meta name="keywords" content="<cms:show page_keywords />">
<meta name="author" content="">

<meta name="robots" content="INDEX, FOLLOW">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<cms:embed "html/facebook-share.php" />
<cms:embed "html/no-touch.php" />
<cms:embed "html/js-vars.php" />

<script src="<cms:show url_assets />js/dist/main-structure.js" async defer></script>
<cms:each custom_js sep=",">
	<script src="<cms:show url_assets />js/dist/<cms:show item />" async defer></script>
</cms:each>

<link rel="stylesheet" type="text/css" href="<cms:show url_assets />css/structure.css">
<cms:each custom_css sep=",">
	<link rel="stylesheet" type="text/css" href="<cms:show url_assets />css/<cms:show item />">
</cms:each>
