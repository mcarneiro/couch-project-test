<cms:editable name="og_group"
	label="Conteúdo para o 'share' (Facebok, Twitter e sites que usam o OpenGraph)"
	type="group"
	order="10" />
<cms:editable name="og_title"
	label="Título"
	desc='metatag "og:title"'
	type="text"
	group="og_group"
	hidden="1"><cms:show og_title /></cms:editable>
<cms:editable name="og_description"
	label="Descrição"
	desc='metatag "og:description"'
	type="text"
	group="og_group"
	hidden="1"><cms:show og_description /></cms:editable>
<cms:editable name="og_image"
	label="Imagem"
	desc='metatag "og:image", será reduzido e recortado em 1200 x 630px'
	type="image"
	group="og_group"
	hidden="1"
	show_preview="1"
	width="1200"
	height="630"
	crop="1"
	preview_width="250"><cms:show og_image /></cms:editable>

<cms:if og_title == "">
	<cms:set og_title="<cms:get_custom_field 'og_title' masterpage='globals.php' />" />
	<cms:if og_title == "">
		<cms:set og_title="<cms:show page_title />" />
	</cms:if>
</cms:if>

<cms:if og_description == "">
	<cms:set og_description="<cms:get_custom_field 'og_description' masterpage='globals.php' />" />
	<cms:if og_description == "">
		<cms:set og_description="<cms:show page_description />" />
	</cms:if>
</cms:if>

<cms:if og_image == "">
	<cms:set og_image="<cms:get_custom_field 'og_image' masterpage='globals.php' />" />
</cms:if>

<cms:if og_url == "">
	<cms:set og_url="<cms:show url_root />" />
</cms:if>

<meta property="og:title" content="<cms:show og_title />" />
<meta property="og:description" content="<cms:show og_description />" />
<meta property="og:type" content="website" />
<meta property="og:image" content="<cms:show og_image />" />
<meta property="og:url" content="<cms:show og_url />" />
