<cms:php>
	global $CTX;
	$val = $CTX->get('hlp_base_button_value');
	$val = preg_replace('/\s/', '-', $val);
	$CTX->set('hlp_base_button_value', $val);
</cms:php>

<cms:if hlp_base_button_value != "">
<button class="base-button">
	<span class="icon"></span>
	<cms:show hlp_base_button_value />
</button>
</cms:if>
