<cms:php>
/*
	Parâmetros
	* hlp_image_id
	* hlp_image_label
	* hlp_image_src
	* hlp_image_alt

	implementação ("<>" substituído por "{}")
		{cms:set hlp_image_id="image"}
		{cms:set hlp_image_label="Imagem"}
		{cms:set hlp_image_src="image/image.jpg"}
		{cms:embed "helpers/admin/image.php"}
*/
</cms:php>

<cms:if hlp_image_id == "">
	<cms:set hlp_image_id="image" />
</cms:if>
<cms:if hlp_image_label == "">
	<cms:set hlp_image_label="<cms:show hlp_image_id />" />
</cms:if>
<cms:if hlp_image_src != "">
	<cms:set image_src="<cms:concat url_assets hlp_image_src />" />
</cms:if>

<cms:set hlp_image_group="<cms:concat hlp_image_id '_group' />" />

<cms:editable name="<cms:show hlp_image_group />"
	label="<cms:show hlp_image_label />"
	type="group" />

<cms:editable name="<cms:show hlp_image_id />"
	type="image"
	label="Caminho"
	show_preview="1"
	preview_width="200"
	hidden="1"
	group="<cms:show hlp_image_group />"><cms:show image_src /></cms:editable>

<cms:editable name="<cms:concat hlp_image_id '_alt' />"
	type="text"
	label="Descritivo (para SEO)"
	group="<cms:show hlp_image_group />"><cms:show hlp_image_alt /></cms:editable>
