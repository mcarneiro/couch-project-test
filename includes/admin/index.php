<cms:editable name="h1"
	label="Título principal"
	type="text">Hello CouchCMS!</cms:editable>
<cms:editable name="intro"
	label="Texto de introdução"
	type="richtext"
	toolbar="custom"
	custom_toolbar="bold, italic">
	<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit.
		Esse accusantium fugit maxime id voluptates aliquam velit!
	</p>
</cms:editable>

<cms:set hlp_image_id="imagem" />
<cms:set hlp_image_label="Imagem de exemplo" />
<cms:set hlp_image_src="image/avatar.jpg" />
<cms:embed "helpers/admin/image.php" />

<cms:set hlp_image_id="photo" />
<cms:set hlp_image_label="Foto de exemplo" />
<cms:embed "helpers/admin/image.php" />