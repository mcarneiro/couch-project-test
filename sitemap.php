<?php require_once( 'couch/cms.php' ); ?>
<cms:content_type 'text/xml' /><cms:concat '<' '?xml version="1.0" encoding="' k_site_charset '"?' '>' />
<cms:template title="Sitemap" hidden="1" executable="1" order="100" />
<urlset
	xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
		http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

	<cms:templates order='asc'>
		<cms:pages masterpage=k_template_name>
			<cms:if k_template_is_executable>
				<url>
					<loc><cms:show k_page_link /></loc>
					<lastmod><cms:date "<cms:if k_page_modification_date='0000-00-00 00:00:00'><cms:show k_page_date /><cms:else /><cms:show k_page_modification_date /></cms:if>" format='Y-m-d\TH:i:s+00:00' gmt='1' /></lastmod>
					<changefreq>daily</changefreq>
				</url>
			</cms:if>
		</cms:pages>
	</cms:templates>
</urlset>
<?php COUCH::invoke(); ?>