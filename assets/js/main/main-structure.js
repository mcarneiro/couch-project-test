define(function (require) {
	// Almond por sempre ser saída concatenada
	require('almond');
	require('zepto/zepto');
	require('zepto/event');
	require('zepto/ie');
	// Arquivo inserido para garantir que 2 arquivos
	// sejam carregados em paralelo e executem na sequencia
	require('modules/hyojun/mod-async-run');
});

// módulo do analytics
require(['modules/hyojun/mod-gaq'], function(modGAQ) {
	modGAQ.runGA(
		hyojun.config.ga.trackCode,
		hyojun.config.ga.trackValue,
		hyojun.config.isLocal
	);
});
