<?php require('couch/cms.php'); ?>
<cms:template title="Configurações gerais" executable="0" order="1">
	<cms:editable name="site_title"
		label="Título do Site"
		desc='meta-tag "title"'
		type="text"
		order="0"></cms:editable>
	<cms:editable name="site_description"
		label="Descrição do Site"
		desc='metatag "description"'
		type="text"
		order="0"></cms:editable>
	<cms:editable name="site_keywords"
		label="Palavras-chave do Site"
		desc='metatag "keywords"'
		type="text"
		order="0"></cms:editable>

	<cms:editable name="og_group"
		label="Conteúdo padrão para o 'share' (Facebok, Twitter e sites que usam o OpenGraph)"
		type="group"
		order="5" />
	<cms:editable name="og_title"
		label="Título"
		desc='metatag "og:title"'
		type="text"
		group="og_group"></cms:editable>
	<cms:editable name="og_description"
		label="Descrição"
		desc='metatag "og:description"'
		type="text"
		group="og_group"></cms:editable>
	<cms:editable name="og_image"
		label="Imagem"
		desc='metatag "og:image", será reduzido e recortado em 1200 x 630px'
		type="image"
		group="og_group"
		show_preview="1"
		width="1200"
		height="630"
		crop="1"
		preview_width="250"></cms:editable>

	<cms:editable name="tracking"
		label="Tracking code"
		desc="Código do Analytics, Tag Manager, etc."
		type="textarea"
		no_xss_check="1"
		order="5"></cms:editable>
</cms:template>
<?php COUCH::invoke(); ?>
