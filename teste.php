<?php require('couch/cms.php'); ?>
<cms:embed "html/tag-html.php" />
<head>
	<?php
		// <cms:set src_page_title="" />
		// <cms:set src_page_description="" />
		// <cms:set src_page_keywords="" />
		// <cms:set custom_css="home.css" />
		// <cms:set custom_js="home.js" />
	?>
	<cms:set src_page_title="Título da página de teste (no código)" />
	<cms:embed "html/tag-head.php" />
	<cms:template title="Teste" clonable="1" order="25" />
</head>
<body>
	<cms:if k_is_page == "0" >
	Home de teste.php

	<cms:pages masterpage="teste.php">
		<li>
			<a href="<cms:show k_page_link />"><cms:show h1 /></a>
		</li>
	</cms:pages>
	<cms:else />
	<cms:embed "structure/header.php" />
	<h1>
		<cms:editable name="h1"
			label="Título principal"
			type="text">Hello CouchCMS!</cms:editable>
	</h1>

	<div>
		<cms:editable name="intro"
			label="Texto de introdução"
			type="richtext"
			toolbar="custom"
			custom_toolbar="bold, italic">
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit.
			Esse accusantium fugit maxime id voluptates aliquam velit!
		</p>
		</cms:editable>
	</div>

	<p>
		teste
	</p>

	<cms:embed "structure/footer.php" />
	<cms:embed "html/tag-foot.php" />
	</cms:if>
</body>
</html>
<?php COUCH::invoke(); ?>
