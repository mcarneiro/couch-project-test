<?php require('couch/cms.php'); ?>
<cms:embed "html/tag-html.php" />
<head>
	<?php
		// <cms:set src_page_title="" />
		// <cms:set src_page_description="" />
		// <cms:set src_page_keywords="" />
		// <cms:set custom_css="home.css" />
		// <cms:set custom_js="home.js" />
	?>
	<cms:embed "html/tag-head.php" />
	<cms:template title="Repeatable" order="25">
		<cms:editable name="h1"
			label="Título principal"
			type="text">Repeatable no CouchCMS</cms:editable>

		<cms:repeatable 'lista_pessoas'>
			<cms:editable name="nome" label="Nome" type="text" />
			<cms:editable name="idade" label="Idade" type="text" />
			<cms:editable name="profissao" label="Profissão" type="text" />
		</cms:repeatable>

		<cms:set lista_pessoas_len="0" />
		<cms:show_repeatable 'lista_pessoas'>
			<cms:set lista_pessoas_len="<cms:show k_count />" 'global' />
		</cms:show_repeatable>
	</cms:template>
</head>
<body>
	<cms:embed "structure/header.php" />
	<h1>
		<cms:show h1 />
	</h1>

	<cms:if lista_pessoas_len gt "0" >
	<p>
		Lista de pessoas
	</p>
	<ul>
		<cms:show_repeatable 'lista_pessoas'>
		<li>
			<strong><cms:show nome /></strong>, <cms:show idade /> anos, <cms:show profissao />.
		</li>
		</cms:show_repeatable>
	</ul>
	</cms:if>

	<cms:embed "structure/footer.php" />
	<cms:embed "html/tag-foot.php" />
</body>
</html>
<?php COUCH::invoke(); ?>
