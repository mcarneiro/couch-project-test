<?php require('couch/cms.php'); ?>
<cms:embed "html/tag-html.php" />
<cms:template title="Home" order="25">
	<cms:embed "admin/index.php" />
</cms:template>
<head>
	<?php
		// <cms:set src_page_title="" />
		// <cms:set src_page_description="" />
		// <cms:set src_page_keywords="" />
		// <cms:set custom_css="home.css" />
		// <cms:set custom_js="home.js" />
	?>
	<cms:embed "html/tag-head.php" />
</head>
<body>
	<cms:embed "structure/header.php" />
	<h1>
		<cms:show h1 /> <cms:show hlp_image_id />

	</h1>
	<div>
		<cms:show intro />
	</div>

	<p>Texto hard coded!</p>

	<cms:set hlp_teste_hw="hello world!" />
	<cms:embed "helpers/teste.php" />

	<cms:set hlp_teste_hw="hello world 2!" />
	<cms:embed "helpers/teste.php" />

	<img src="<cms:show photo />" />
	<img src="<cms:thumbnail photo width='200' />" />
	<img src="<cms:thumbnail photo width='100' />" />
	<img src="<cms:thumbnail photo width='200' height='100' crop='1' />" />

	<cms:embed "structure/footer.php" />
	<cms:embed "html/tag-foot.php" />
</body>
</html>
<?php COUCH::invoke(); ?>
