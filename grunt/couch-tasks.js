module.exports = function(grunt) {

	'use strict';

	var path = require('path');
	var archiver = require('archiver');
	var unzip = require('unzip2');
	var moment = require('moment');
	var fs = require('fs');
	var url = require('url');
	var couch = require('./modules/couch-bridge.js')(grunt);
	var dateFormat = 'YYYYMMDD-HHmmss';
	var bkp = 'bkp' + path.sep;

	// "PRIVATE" TASKS (escondendo do help)
	if (!grunt.option('help')) {

		grunt.registerTask('couch-install',
			[
				'Acessa o site com os dados padrão de usuário e senha. Opções possíveis:',
				'* --user - nome do usuário (padrão system)',
				'* --password - senha do usuário (padrão admin)',
				'* --url - url do servidor (padrão http://localhost:1144/).'
			].join('\n'),
			function() {
				var done = this.async();
				var data = {
					uri: couch.url(),
					method: 'POST',
					form: {
						'name': couch.user(),
						'password': couch.password(),
						'repeat_password': couch.password(),
						'email': 'admin@admin.com',
						'k_hid_frm_login': 'frm_login'
					}
				};
				couch.request(data)
					.then(function(res) {
						grunt.log.ok([
							'Couch instalado com sucesso. Acesse',
							couch.url() + 'couch/adm.php ,',
							'entre com o usuário',
							'"' + couch.user() + '"',
							'e senha',
							'"' + couch.password() + '"'
						].join(' '));
						done();
					})
					.catch(couch.error);
			});

		grunt.registerTask('get-dump',
			'Baixa o banco de dados para um futuro restore.',
			function() {
				var done = this.async();
				couch.login().then(function() {
					return couch.request(couch.url() + 'couch/gen_dump.php')
						.then(function(res) {
							var dest = bkp + 'couch' + path.sep + 'install-ex.php';
							grunt.file.write(dest, res.body);
							done();
						});
				})
				.catch(couch.error.bind(couch, done));
			});

		grunt.registerTask('fetch-bkp',
			'Executa a task fetch com opções --dest=bkp e --images-only',
			function() {
				if (grunt.file.exists(bkp)) {
					grunt.file.delete(bkp);
				}
				grunt.option('dest', bkp);
				grunt.option('images-only', true);
				grunt.option('override-assets', true);
				grunt.task.run('fetch');

				grunt.config('copy.upload.dest', path.join(couch.dest(), 'assets/image'));
				grunt.task.run('copy:upload');
			});

		grunt.registerTask('generate-backup-zip',
			'Gera o zip da pasta temporária "bkp/" no formato couch-bkp_*.zip',
			function() {
				var done = this.async();
				var date = moment().format(dateFormat);
				var archive = require('archiver').create('zip');
				var host = url.parse(couch.url()).hostname;
				var name = ['couch-bkp'];

				if (host) {
					name.push(host);
				}
				name.push(date);
				name = name.join('_');

				var output = fs.createWriteStream(name + '.zip');

				grunt.verbose.writeln('Criando ZIP: ' + name);

				output.on('close', function() {
					grunt.file.delete(bkp);
					done();
				});
				output.on('error', function(err) {
					grunt.fail.warn([
						'Não foi possível criar o arquivo ZIP. Erro original:',
						typeof err === 'object' ? JSON.stringify(err) : err
					].join('\n'));
					done();
				});

				archive.pipe(output);
				archive.bulk([
					{
						expand: true,
						cwd: bkp,
						src: '**/*',
						dest: '.'
					}
				]);
				archive.finalize();
			});

		grunt.registerTask('restore-zip',
			'Restaura o ZIP no formato couch-bkp_*.zip',
			function() {
				var done = this.async();

				var file = grunt.option('file');
				var extract = unzip.Extract({
					path: '.'
				});

				if (!file) {
					file = grunt.file.expand({}, [
						"couch-bkp_*.zip"
					]);

					file = file.sort(function(a, b) {
						a = moment(a.match(/\d{8}-\d{6}/).pop(), dateFormat);
						b = moment(b.match(/\d{8}-\d{6}/).pop(), dateFormat);
						return a > b;
					});

					file = file.slice(-2, -1).pop();
				}

				if (!file) {
					grunt.fail.warn([
						'Não foi possível encontrar um arquivo zip no formato couch-bkp_*.zip.',
						'Para apontar um arquivo zip, utilize o parâmetro --file.'
					].join('\n'));
					return done();
				} else {
					grunt.verbose.writeln('Extraindo o arquivo ', file);
				}

				extract.on('close', done);
				extract.on('error', function(err) {
					grunt.fail.warn([
						'Não foi possível extrair o arquivo ZIP. Erro original:',
						typeof err === 'object' ? JSON.stringify(err) : err
					].join('\n'));
					done();
				});

				fs.createReadStream(file).pipe(extract);
			});

		grunt.registerTask('clean-restore',
			'Apaga os arquivos gerados pelo restore.',
			function() {
				var dump = 'couch' + path.sep + 'install-ex.php';
				if (grunt.file.exists(dump)) {
					grunt.file.delete(dump);
				}
			});
	}

	grunt.registerTask('build',
		[
			'Força a atualização do admin e banco de dados acessando as páginas (php) necessárias;',
			'* --user - nome do usuário (padrão system)',
			'* --password - senha do usuário (padrão admin)',
			'* --url - url do servidor (padrão http://localhost:1144/).'
		].join('\n'),
		function(file) {
			var done = this.async();

			function touchPages(pageList) {
				grunt.verbose.writeln('Acessando ', pageList);
				couch.login().then(function() {
					pageList = pageList.map(function(val) {
						return couch.request(val);
					});

					// apenas bate no servidor
					return Promise
						.all(pageList)
						.then(done);
				})
				.catch(couch.error.bind(couch, done));
			}

			file = file || grunt.config('build.file');

			if (file) {
				touchPages([couch.url() + file.replace(process.cwd() + path.sep, '')]);
				return;
			}

			couch.getPHPFiles()
				.then(function(pageList) {
					touchPages(pageList);
				})
				.catch(couch.error.bind(couch, done));
		});

	grunt.event.on("watch", function(action, filepath, target) {
		if (target === "build") {
			grunt.config("build.file", filepath);
		}
	});
};
