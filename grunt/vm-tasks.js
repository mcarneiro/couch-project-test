module.exports = function(grunt) {

	'use strict';

	var couch = require('./modules/couch-bridge.js')(grunt);
	var connectionStringPath = 'couch/config.php';
	var tcDeployConfigPath = 'couch/addons/tc-deploy/tc-deploy-config.json';
	var ssh = [
			'ssh',
			'-p ' + couch.sshPort(),
			'-t',
			'-o Compression=yes',
			'-o DSAAuthentication=yes',
			'-o LogLevel=FATAL',
			'-o StrictHostKeyChecking=no',
			'-o UserKnownHostsFile=/dev/null',
			'-o IdentitiesOnly=yes',
			'-i ' + couch.sshKey(),
			couch.sshUser() + '@' + couch.sshUrl()
		];

	grunt.config.set('exec', {
		'npm': 'npm install',
		'bower': 'bower install -F',
		'bundle': 'bundle install',
		'touch': 'touch noop.config',
		'vm-start': 'vagrant up',
		'vm-fix-perm': ssh.concat([
			'"sudo chgrp -R www-data ' + couch.sshRemotePath() + ' && ',
			'sudo chmod -R 777 ' + couch.sshRemotePath() + '"'
		]).join(' '),
		'vm-mysql': ssh.concat([
			'"mysql -u' + couch.dbUser() + ' --password=' + couch.dbPassword(),
			'-e \'create database if not exists ' + couch.dbName() + '\'"'
		]).join(' '),
		'vm-mysqldrop': ssh.concat([
			'"mysql -u' + couch.dbUser() + ' --password=' + couch.dbPassword(),
			'-e \'drop database if exists ' + couch.dbName() + '\'"'
		]).join(' '),
		'sync': [
			'rsync',
			'-rvuzW',
			'--chmod=ugo=rwx',
			'--no-o --no-g',
			'--delete',
			'--exclude-from=.rsync-ignore',
			'-e "'
		]
		.concat(ssh.slice(0, -1))
		.concat([
			'"',
			'.',
			couch.sshUser() + '@' + couch.sshUrl() + ':' + couch.sshRemotePath()
		]).join(' ')
	});

	grunt.config('exec.sync-all', grunt.config('exec.sync').replace('-rvuzW', '-rvzW'));
	grunt.config('exec.sync-upload', grunt.config('exec.sync')
		.replace('--delete ', '')
		.replace(' . ', ' assets/image ')
		.replace(couch.sshRemotePath(), couch.sshRemotePath() + '/assets'));

	grunt.config.set('watch.sync',{
		files: [
			'assets/css/**/*.css',
			'**/*.config',
			'assets/js/dist/**/*.js',
			'assets/img/**/*.*',
			'assets/fonts/**/*.*',
			'Views/**/*.*',
			'App_Code/**/*.*',
			'!noop.config'
		],
		tasks: ['sync', 'exec:touch']
	});

	grunt.registerTask('sync',
		[
			'Sincroniza apenas os arquivos modificados para a máquina virtual.',
			'* --ssh-key - local do arquivo com a chave privada (padrao .vagrant/.../privatekey)',
			'* --ssh-url - URL do SSH (padrao 127.0.0.1)',
			'* --ssh-port - porta do SSH (padrao 2222)',
			'* --ssh-user - Usuário do SSH (padrao vagrant)',
			'* --ssh-remote-path - Caminho remoto no SSH (padrao /var/www/html)'
		].join('\n'),
		['exec:sync', 'exec:sync-upload', 'exec:vm-fix-perm']);

	grunt.registerTask('sync:all',
		'Envia todos os arquivos do projeto para a máquina virtual;',
		['exec:sync-all', 'exec:vm-fix-perm']);

	grunt.registerTask('update-config',
		[
			'Atualiza os dados do site no config.php.',
			'* --url - variável K_SITE_URL (padrão http://localhost:1144/)',
			'* --db-user - variável K_DB_USER (padrão root)',
			'* --db-password - variável K_DB_PASSWORD (padrão 159753)',
			'* --db-host - variável K_DB_HOST (padrão localhost)',
			'* --db-name - variável K_DB_NAME (padrão ' + grunt.config('paths.project') + ')'
		].join('\n'),
		function() {
			grunt.file.preserveBOM = true;
			var content = grunt.file.read(connectionStringPath)
				.replace(/('K_SITE_URL', ')(.*?)'/, '$1' + couch.url() + '\'')
				.replace(/('K_DB_NAME', ')(.*?)'/, '$1' + couch.dbName() + '\'')
				.replace(/('K_DB_USER', ')(.*?)'/, '$1' + couch.dbUser() + '\'')
				.replace(/('K_DB_PASSWORD', ')(.*?)'/, '$1' + couch.dbPassword() + '\'')
				.replace(/('K_DB_HOST', ')(.*?)'/, '$1' + couch.dbHost() + '\'');
			grunt.file.write(connectionStringPath, content);
		});

	grunt.registerTask('update-tc-deploy-config',
		[
			'Atualiza os dados do site no config.php.',
			'* --tc-api-url - endereço da RESTAPI do TeamCity;',
			'* --tc-build-id - ID da task do TeamCity que faz o publish;',
			'* --tc-project-id - ID do projeto no TeamCity;',
			'* --tc-auth - Base64 do basic authentication do TeamCity;',
			'* --tc-confirm - Mensagem de confirmação para publicação no TeamCity;'
		].join('\n'),
		function() {
			grunt.file.preserveBOM = true;
			var content = grunt.file.read(tcDeployConfigPath)
				.replace(/("api-root":.*?")(.*?)(")/g, '$1' + (couch.tcApiUrl() || '$2') + '$3')
				.replace(/("build-id":.*?")(.*?)(")/g, '$1' + (couch.tcBuildId() || '$2') + '$3')
				.replace(/("project-id":.*?")(.*?)(")/g, '$1' + (couch.tcProjectId() || '$2') + '$3')
				.replace(/("auth":.*?")(.*?)(")/g, '$1' + (couch.tcAuth() || '$2') + '$3')
				.replace(/("confirm-message":.*?")(.*?)(")/g, '$1' + (couch.tcConfirm() || '$2') + '$3');
			grunt.file.write(tcDeployConfigPath, content);
		});

	grunt.registerTask('packages',
		'Instala os pacotes necessários para rodar o projeto;',
		['exec:npm', 'exec:bower', 'exec:bundle']);

	grunt.registerTask('reset',
		'Apaga o banco de dados e a instalação do CouchCMS na máquina virtual e reinstala.',
		['exec:vm-mysqldrop', 'setup']);
};
