module.exports = function(grunt) {

	'use strict';

	var path = require('path');
	var request = require('request');
	var fs = require('fs');
	var url = require('url');
	var util = require('util');
	var couch = require('./modules/couch-bridge.js')(grunt);
	var extensionRegExp = '("|\'|url\\()[^"\'\\(]+\\.(%s).*?(\\1|\\))';

	var getConfig = function() {
		if (grunt.file.exists('go-static.config.json')) {
			return grunt.file.readJSON('go-static.config.json');
		}
		return {convert: {}, ignore: [], version: {replace:'', ext: []}, download: {ext: []}};
	};

	var urlToRegExp = function(val) {
		return val && val.replace(/([\/\?\.])/g, '\\$1');
	};

	var err = function(fn) {
		return function(err) {
			err = typeof err != "string" ? JSON.stringify(err) : err;
			grunt.fail.warn('[' + fn + '] ' + err);
		};
	};

	// "PRIVATE" TASKS (escondendo do help)
	if (!grunt.option('help')) {

		grunt.registerTask('clear-static',
			'Limpa qualquer versão anterior de um site gerado.',
			function() {
				if (grunt.file.exists(couch.dest())) {
					grunt.file.delete(couch.dest());
				}
			});

		grunt.registerTask('apply-version',
			'Aplica a versão para os assets automaticamente.',
			function() {
				var config = getConfig();
				var version = couch.version();
				var expandList = config.version.parse.map(function(val) {
					return couch.dest() + '**/*.' + val;
				});
				var fileList = grunt.file.expand({}, [
					couch.dest() + '**/*' + couch.extension()
				].concat(expandList));

				function applyVersion(file) {
					var content = grunt.file.read(file);
					var regExp = new RegExp(util.format(extensionRegExp, config.version.ext.join('|')), 'gmi');
					var matches = content.match(regExp) || [];

					content = content.replace(new RegExp(config.version.replace, 'gm'), version);

					matches.forEach(function(val, i) {
						var urlValue = val.slice(1, -1);
						var urlData = url.parse(urlValue, true);
						var group1 = '\\' + val.slice(0, 1);
						var group2 = '\\' + val.slice(-1);
						var regExp = util.format('(%s)%s(%s)', group1, urlToRegExp(urlValue), group2);

						delete urlData.search;
						urlData.query = urlData.query || {};
						urlData.query.v = version;

						content = content.replace(new RegExp(regExp, 'g'), '$1' + url.format(urlData) + '$2');
					});

					grunt.file.write(file, content);
				}

				fileList.forEach(applyVersion);
			});
	}

	grunt.registerTask('fetch',
		'Baixa a versão HTML do site + imagens.',
		function() {
			var config = getConfig();
			var downloadList = [];
			var cssList = [];
			var done = this.async();
			var imageOnly = grunt.option('images-only') === true;
			var overrideAssets = grunt.option('override-assets') === true;

			function getPage(page) {
				var file = page.replace(couch.url(), '');

				return couch.request(page)
					.then(function(req) {
						return {
							page: page,
							body: req.body
						};
					})
					.catch(err('getPage'));
			}

			function isPageIgnored(file) {
				var isIgnored = false;
				if (config.ignore) {
					config.ignore.forEach(function(val) {
						val = /^\/.*?\/$/.test(val) ? new RegExp(val.slice(1, -1)) : val;

						if (file.match(val)) {
							isIgnored = true;
						}
					});
				}
				return isIgnored;
			}

			function convertFileName(file) {
				if (!!config.convert[file]) {
					return config.convert[file];
				} else if (!file.match(/\.\w{1,4}$/)) {
					return file + 'index' + couch.extension();
				}
				return file;
			}

			function getRelativeProdPath(file) {
				var urlProd = couch.urlProd();
				var rel = [];

				if (urlProd.match(/^\./)) {
					rel.length = file.split(path.sep).length;

					return rel.join('../') || urlProd;
				}

				return urlProd;
			}

			function beforeParsePage(body) {
				var parse;
				if (grunt.option('before-parse')) {
					parse = require('../' + grunt.option('before-parse'));
					return parse(body, couch);
				}
				return body;
			}

			function convertFileNameInPage(body) {
				var keys = Object.keys(config.convert);

				keys.forEach(function(val) {
					var newUrl = url.resolve(couch.url(), val);
					var regExp = new RegExp(urlToRegExp(newUrl) + '(?![^\\?\'"# ])', 'g');
					body = body.replace(regExp, url.resolve(couch.url(), config.convert[val]));
				});

				return body;
			}

			function parsePage(body, urlProd) {
				var urlAsRegExp = urlToRegExp(couch.url());
				var metaRegExp = new RegExp('(<meta.*?)' + urlAsRegExp + '(.*?>)', 'g');

				return convertFileNameInPage(body)
					.replace(metaRegExp, '$1' + couch.urlMeta() + '$2')
					.replace(new RegExp(urlAsRegExp, 'g'), urlProd);
			}

			function savePage(data) {
				var file = data.page.replace(couch.url(), '');
				var body = data.body;
				var urlProd = getRelativeProdPath(file);

				if (isPageIgnored(file)) {
					grunt.verbose.writeln('Ignorando download da página: ', file);
					return data;
				}

				grunt.verbose.writeln('Fazendo Download da página: ', file);

				file = convertFileName(file);
				body = parsePage(beforeParsePage(body), urlProd);

				grunt.file.write(couch.dest() + file, body);

				return data;
			}

			function queueImagesDownload(data) {
				var regExp = new RegExp(util.format(extensionRegExp, config.download.ext.join('|')), 'gmi');
				var list = data.body.match(regExp) || [];

				list.forEach(function(val) {
					var finalUrl = url.parse(val.slice(1, -1));

					delete finalUrl.search;
					delete finalUrl.hash;
					delete finalUrl.query;

					if (data.page) {
						val = url.resolve(data.page, url.format(finalUrl));
					}

					if (downloadList.indexOf(val) < 0) {
						downloadList.push(val);
					}
				});

				return data;
			}

			function queueCSSDownload(data) {
				var list = data.body.match(/[^"' ]+\.css(\?[^"' ]+)?/gmi) || [];

				list.forEach(function(val) {
					if (cssList.indexOf(val) < 0) {
						cssList.push(val);
					}
				});

				return data;
			}

			function readCSS() {
				var list = cssList.map(function(url) {
					return couch.request(url)
						.then(function(res) {
							return queueImagesDownload({
								page: url,
								body: res.body
							});
						});
				});

				return Promise.all(list).catch(err('readCSS'));
			}

			function downloadImages() {
				var list = downloadList.map(function(val) {
					var dest = val.replace(couch.url(), '');

					if (!overrideAssets && grunt.file.exists(dest)) {
						grunt.verbose.writeln('Arquivo ', dest, 'existe. Ignorando...');
						return Promise.resolve();
					}

					return new Promise(function(ok, fail) {
						grunt.verbose.writeln('Downloading: ', val, 'into', dest);
						var data = {
							url: val,
							encoding: null
						};
						return request(data, function(err, res) {
							if (!err && res.statusCode === 200 && res.headers['content-type'].match(/^image/)) {
								grunt.file.write(dest, res.body);
								return ok();
							}
							return fail({
								err: err,
								url: val,
								contentType: res.headers['content-type'],
								status: res.statusCode
							});
						});
					});
				});

				return Promise.all(list).catch(err('downloadImages'));
			}

			function processPageList(pageList) {
				pageList = pageList.map(function(val) {
					return getPage(val)
						.then(function(data) {
							if (!imageOnly) {
								return savePage(data);
							}
							return data;
						})
						.then(queueImagesDownload)
						.then(queueCSSDownload)
						.catch(err('processPageList'));
				});

				// faz o download das páginas e imagens
				return Promise.all(pageList)
					.then(readCSS)
					.then(downloadImages)
					.then(done)
					.catch(err('task fetch'));
			}

			couch.getPageList()
				.then(function(pageList) {
					if (pageList.length === 0) {
						throw new Error([
							'Nenhuma página foi cadastrada no CouchCMS!',
							'Rode "grunt build" para registrá-las.'
						].join('\n'));
					}
					return processPageList(pageList);
				})
				.catch(couch.error.bind(couch, done));
		});

	grunt.config('copy.assets', {
		cwd: 'assets/',
		src: [
			'**/*',
			'!sass',
			'!sass/**/*',
			'!js/**/*',
			'js/dist/**/*',
		],
		dest: path.join(couch.dest(), 'assets'),
		expand: true
	});

	grunt.config('copy.upload', {
		cwd: 'assets/image',
		src: ['**/*'],
		dest: path.join(couch.dest(), 'assets/image'),
		expand: true
	});

	grunt.config('htmlmin.static', {
		options: {
			caseSensitive: true,
			collapseBooleanAttributes: false,
			collapseWhitespace: true,
			conservativeCollapse: true,
			customAttrAssign: [],
			customAttrCollapse: '',
			customAttrSurround: [],
			ignoreCustomComments: [],
			keepClosingSlash: true,
			lint: false,
			maxLineLength: 120,
			minifyCSS: true,
			minifyJS: true,
			minifyURLs: false,
			preserveLineBreaks: false,
			preventAttributesEscaping: false,
			processScripts: [],
			removeAttributeQuotes: false,
			removeCDATASectionsFromCDATA: false,
			removeComments: true,
			removeCommentsFromCDATA: true,
			removeEmptyAttributes: false,
			removeEmptyElements: false,
			removeIgnored: false,
			removeOptionalTags: false,
			removeRedundantAttributes: false,
			removeScriptTypeAttributes: false,
			removeStyleLinkTypeAttributes: false,
			useShortDoctype: false
		},
		cwd: couch.dest(),
		src: [
			'**/*' + couch.extension(),
			'**/*.xml'
		],
		dest: couch.dest(),
		expand: true
	});

	grunt.registerTask('go-static',
		[
			'Gera os arquivos estáticos do site e copia para a pasta _site/. Opções possíveis:',
			'* --override-assets - sobrepõe as imagens locais com as versões atualizadas do servidor (padrão false).',
			'* --url - url do servidor (padrão http://localhost:1144/).',
			'* --url-prod - url que será utilizada nos htmls estáticos (padrão ./).',
			'* --url-meta - url absoluta para ser utilizada em metatags (padrão valor de url-prod).',
			'* --dest - diretório de destino onde o site estático será gerado (padrão _site).',
			'* --override-assets - sobrepõe imagens localmente (padrão false);',
			'* --images-only - não gera os arquivos .html (padrão false);',
			'* --extension - extensão padrão para páginas de saída (padrão .html);',
			['* --before-parse - módulo js, relativo ao gruntfile.js, que será executado antes de qualquer',
			'replace nos arquivos de saída. Recebe a string do body e deve retornar o valor alterado;'].join(' ')
		].join('\n'),
		['clear-static', 'fetch', 'copy:assets', 'apply-version', 'htmlmin:static']);
};
