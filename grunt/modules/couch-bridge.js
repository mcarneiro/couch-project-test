module.exports = function(grunt, opt) {

	var request = require('request');
	var path = require('path');
	var config = null;
	var vmID = null;

	request = request.defaults({jar: true});

	return {
		vmID: function() {
			if (vmID) {
				return vmID;
			}

			var path = '.vagrant/machines/default/virtualbox/id';
			if (grunt.file.exists(path)) {
				vmID = grunt.file.read(path);
			}

			return vmID;
		},
		checkVM: function() {
			if (!this.vmID()) {
				return config || {};
			}

			try {
				var exec = require('child_process').execSync;
				var stdout = exec('VboxManage showvminfo ' + this.vmID()).toString();

				var matches = stdout.match(/^NIC \d Rule\(\d\).*$/gm);
				config = {};

				matches.forEach(function(val) {
					var name = val.match(/name = ([\w\d]+)/).pop();
					var host = val.match(/host port = ([\w\d]+)/).pop();

					switch(name) {
						case 'http':
							config.port = host;
							break;
						case 'ssh':
							config['ssh-port'] = host;
							break;
					}
				});
			} catch(e) {
				grunt.verbose.writeln('O comando "VboxManage" não existe ou retornou erro. Erro original:\n' + e);
			}

			return config || {};
		},
		config: function() {
			if (config) {
				return config;
			}

			return this.checkVM();
		},
		port: function() {
			return grunt.option('port') || this.config().port || '1144';
		},
		url: function() {
			return grunt.option('url') || 'http://localhost:' + this.port() + '/';
		},
		urlProd: function() {
			return grunt.option('url-prod') || './';
		},
		urlMeta: function() {
			return grunt.option('url-meta') || this.urlProd();
		},
		user: function() {
			return grunt.option('user') || 'system';
		},
		password: function() {
			return grunt.option('password') || 'admin';
		},

		dbUser: function() {
			return grunt.option('db-user') || 'root';
		},
		dbPassword: function() {
			return grunt.option('db-password') || '159753';
		},
		dbHost: function() {
			return grunt.option('db-host') || 'localhost';
		},
		dbName: function() {
			return grunt.option('db-name') || grunt.config('paths.project');
		},

		sshKey: function() {
			return grunt.option('ssh-key') || '.vagrant/machines/default/virtualbox/private_key';
		},
		sshUrl: function() {
			return grunt.option('ssh-url') || '127.0.0.1';
		},
		sshPort: function() {
			return grunt.option('ssh-port') || this.config()['ssh-port'] || '2222';
		},
		sshUser: function() {
			return grunt.option('ssh-user') || 'vagrant';
		},
		sshRemotePath: function() {
			return grunt.option('ssh-remote-path') || '/var/www/html';
		},

		tcApiUrl: function() {
			return grunt.option('tc-api-url');
		},
		tcBuildId: function() {
			return grunt.option('tc-build-id');
		},
		tcProjectId: function() {
			return grunt.option('tc-project-id');
		},
		tcAuth: function() {
			return grunt.option('tc-auth');
		},
		tcConfirm: function() {
			return grunt.option('tc-confirm');
		},

		extension: function() {
			return grunt.option('extension') || '.html';
		},
		dest: function() {
			return path.normalize(grunt.option('dest') || '_site') + path.sep;
		},
		version: function() {
			return grunt.option('ver') || '1';
		},

		error: function(done, err) {
			grunt.fail.warn([
				'Não foi possível conectar à página em ' + this.url() + '. Causas mais comuns:',
				'- Erro 500 (include apontado pro local errado, erro de sintaxe do PHP, etc.);',
				'- VM não está rodando;',
				'- Vagrant sobrepos a porta de 1144 para outro valor (2 VMs rodando ao mesmo tempo).',
				'', 'Mensagem de erro original: ',
				typeof err === 'object' ? JSON.stringify(err) : err
			].join('\n'));
			done();
		},

		convertPageListResponse: function(body) {
			var pageList = body.match(/page:.*?$/gm) || [];
			pageList = pageList.map(function(val) {
				return val.replace(/^page:/, '');
			});
			return pageList;
		},

		getPageList: function() {
			return this.login().then(function() {
				return this.request(this.url() + 'page-list/')
					.then(function(res) {
						var pageList = res.body.match(/page:.*?$/gm) || [];
						pageList = pageList.map(function(val) {
							return val.replace(/^page:/, '');
						});
						grunt.verbose.writeln('Páginas registradas no CouchCMS: ' + pageList);
						return pageList;
					});
			}.bind(this));
		},

		getPHPFiles: function() {
			return new Promise(function(ok, fail) {
				try {
					var files = grunt.file.expand({
						filter: function(file) {
							return /COUCH::invoke/.test(grunt.file.read(file));
						}
					}, [
						"**/*.php",
						"!couch/**/*.php",
						"!includes/**/*.php"
					]);

					files = files.map(function(val) {
						return this.url() + val;
					}.bind(this));

					grunt.verbose.writeln([
						'Páginas relacionadas com o CouchCMS:',
						files.join('\n')
					].join('\n'));

					ok(files);
				} catch(e) {
					fail(e);
				}
			}.bind(this));
		},

		request: function(opt) {
			return new Promise(function(ok, fail) {
				grunt.verbose.writeln('requesting: ', opt);
				request(opt, function(err, res, body) {
					if (!err && ((res.statusCode >= 200 && res.statusCode < 400) || res.statusCode === 404)) {
						if (res.statusCode === 404) {
							grunt.log.subhead([
								'> ' + opt + ' será ignorado por não ter sido encontrado.',
								'  Se a ação foi proposital, acesse o admin e remova o template.',
								'  Se não, verifique se a VM está rodando e se as portas estão corretas.'
							].join('\n'));
						}
						return ok(res);
					}
					return fail({
						err: err,
						res: res,
						body: body
					});
				});
			});
		},

		login: function() {
			return new Promise(function(ok, fail) {
				var data = {
					uri: this.url() + 'couch/login.php',
					qs: {
						redirect: '/couch/adm.php'
					},
					method: 'POST',
					headers: {
						'Cookie': 'couchcms_testcookie=CouchCMS+test+cookie'
					},
					form: {
						'k_user_name': this.user(),
						'k_user_pwd': this.password(),
						'k_cookie_test': '1',
						'k_login': 'Login'
					}
				};
				this.request(data)
					.then(ok)
					.catch(fail);
			}.bind(this));
		}
	};
};
